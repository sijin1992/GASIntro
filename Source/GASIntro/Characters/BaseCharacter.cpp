// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"
#include "AbilitySystemComponent.h"
#include "GASIntro/Gameplay/BaseGameplayAbility.h"
#include "GASIntro/Gameplay/BaseAttributeSet.h"

// Sets default values
ABaseCharacter::ABaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>("AbilitySystemComponent");
	AttributeSet = CreateDefaultSubobject<UBaseAttributeSet>("AttributeSet");

}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	AddGameplayTag(FullHealthTag, 1);	
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UAbilitySystemComponent* ABaseCharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

FGameplayAbilityInfo ABaseCharacter::AquireAbility(TSubclassOf<UBaseGameplayAbility> Ability)
{
	if (AbilitySystemComponent && Ability)
	{
		AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(Ability)); //使用FGameplayAbilitySpec实例化Ability
		AbilitySystemComponent->InitAbilityActorInfo(this, this);//初始化能力信息，单机参数都是自己，如果跟网络相关，参数一个是Playerstadium,一个是Character类

		UBaseGameplayAbility* AbilityInstance = Ability.Get()->GetDefaultObject<UBaseGameplayAbility>();
		if (AbilityInstance)
		{
			return AbilityInstance->GetAbilityInfo();
		}
	}
	return FGameplayAbilityInfo();
}

void ABaseCharacter::AddGameplayTag(FGameplayTag& Tag, int Count)
{
	//添加松散的Tag，所谓的“松散的”是指从代码添加的Tag而非GameEffect添加的Tag
	GetAbilitySystemComponent()->AddLooseGameplayTag(Tag, Count);
}

void ABaseCharacter::RemoveGameplayTag(FGameplayTag& Tag, int Count)
{
	if (Count <= 0)
	{
		GetAbilitySystemComponent()->SetTagMapCount(Tag, 1);
		GetAbilitySystemComponent()->RemoveLooseGameplayTag(Tag, 1);
	}
	else
	{
		GetAbilitySystemComponent()->RemoveLooseGameplayTag(Tag, Count);
	}
}
