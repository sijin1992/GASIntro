// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "BaseGameplayAbility.generated.h"

//技能消耗类型
UENUM(BlueprintType)
enum class ECostType : uint8
{
	Health,
	Mana,
	Strength,
};

//技能结构体
USTRUCT(BlueprintType)
struct FGameplayAbilityInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite,Category = "BaseGameplayAbility")
	float Cooldown;//技能冷却

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseGameplayAbility")
	float Cost;//技能消耗

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseGameplayAbility")
	ECostType CostType;//技能消耗类型

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseGameplayAbility")
	UMaterialInstance* IconMaterial;//技能图标

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseGameplayAbility")
	TSubclassOf<UBaseGameplayAbility> AbilityClass;//技能类名

	FGameplayAbilityInfo();

	FGameplayAbilityInfo(float InCooldown, float InCost, ECostType InCostType, UMaterialInstance* InIconMaterial, TSubclassOf<UBaseGameplayAbility> InAbilityClass);
};
/**
 * 
 */
UCLASS()
class GASINTRO_API UBaseGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseGameplayAbility")
	UMaterialInstance* IconMaterial;//技能图标

public:
	UFUNCTION(BlueprintCallable, Category = "BaseGameplayAbility")
	FGameplayAbilityInfo GetAbilityInfo();
};
