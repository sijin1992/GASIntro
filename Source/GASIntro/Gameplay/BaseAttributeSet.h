// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "BaseAttributeSet.generated.h"

/**
 * 
 */


//宏定义一个两个参数的委托OnAttributeChangeDelegate
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAttributeChangeDelegate, float, CurrentValue, float, MaxValue);

UCLASS()
class GASINTRO_API UBaseAttributeSet : public UAttributeSet
{
	GENERATED_BODY()
	
public:
	UBaseAttributeSet();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseAttributeSet")
	FGameplayAttributeData MaxHealth;//最大生命值

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseAttributeSet")
	FGameplayAttributeData Health;//初始生命值

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseAttributeSet")
	FGameplayAttributeData MaxMana;//最大法力值

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseAttributeSet")
	FGameplayAttributeData Mana;//初始法力值

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseAttributeSet")
	FGameplayAttributeData MaxStrength;//最大体力值

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseAttributeSet")
	FGameplayAttributeData Strength;//初始体力值

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseAttributeSet")
	FGameplayAttributeData Attack;//攻击力

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BaseAttributeSet")
	FGameplayAttributeData Armor;//护甲值

	UPROPERTY(BlueprintAssignable)
	FOnAttributeChangeDelegate OnHealthChange;//声明一个委托代理,BlueprintAssignable在蓝图中可指定

	UPROPERTY(BlueprintAssignable)
	FOnAttributeChangeDelegate OnManaChange;

	UPROPERTY(BlueprintAssignable)
	FOnAttributeChangeDelegate OnStrengthChange;

	void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData& Data) override;

};
