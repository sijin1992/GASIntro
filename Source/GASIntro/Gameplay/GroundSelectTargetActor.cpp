// Fill out your copyright notice in the Description page of Project Settings.


#include "GroundSelectTargetActor.h"
#include "GameFramework/PlayerController.h"
#include "Abilities/GameplayAbility.h"

void AGroundSelectTargetActor::StartTargeting(UGameplayAbility* Ability)
{
	Super::StartTargeting(Ability);

	PrimaryPC = Cast<APlayerController>(Ability->GetOwningActorFromActorInfo()->GetInstigatorController());
}

void AGroundSelectTargetActor::ConfirmTargetingAndContinue()
{
	FVector LookingPoint;
	GetPlayerLookingPoint(LookingPoint);

	TArray<FOverlapResult> OverlapResults;
	TArray<TWeakObjectPtr<AActor>> OverlapedActors;

	FCollisionQueryParams QueryParams;
	QueryParams.bTraceComplex = false;
	QueryParams.bReturnPhysicalMaterial = false;
	APawn* PrimaryPawn = PrimaryPC->GetPawn();
	if (PrimaryPawn)
	{
		QueryParams.AddIgnoredActor(PrimaryPawn);
	}

	bool QueryResult = GetWorld()->OverlapMultiByObjectType(
		OverlapResults,
		LookingPoint,
		FQuat::Identity,
		FCollisionObjectQueryParams(ECollisionChannel::ECC_Pawn),
		FCollisionShape::MakeSphere(SelectRadius),
		QueryParams
	);

	if (QueryResult)
	{
		for (int i = 0; i < OverlapResults.Num(); i++)
		{
			APawn* OverlappedPawn = Cast<APawn>(OverlapResults[i].GetActor());

			if (OverlappedPawn && !OverlapedActors.Contains(OverlappedPawn))
			{
				OverlapedActors.Add(OverlappedPawn);
			}
		}
	}

	//仿写父类方法
	FGameplayAbilityTargetDataHandle TargetDataHandle;

	FGameplayAbilityTargetData_LocationInfo* CenterLocation = new FGameplayAbilityTargetData_LocationInfo();

	CenterLocation->TargetLocation.LiteralTransform = FTransform(LookingPoint);
	CenterLocation->TargetLocation.LocationType = EGameplayAbilityTargetingLocationType::LiteralTransform;
	TargetDataHandle.Add(CenterLocation);

	if (OverlapedActors.Num() > 0)
	{
		FGameplayAbilityTargetData_ActorArray* ActorArray = new FGameplayAbilityTargetData_ActorArray();
		ActorArray->SetActors(OverlapedActors);
		TargetDataHandle.Add(ActorArray);
	}

	check(ShouldProduceTargetData());
	if (IsConfirmTargetingAllowed())
	{
		TargetDataReadyDelegate.Broadcast(TargetDataHandle);
	}
}

bool AGroundSelectTargetActor::GetPlayerLookingPoint(FVector& out_LookingPoint)
{
	FVector ViewLocation;
	FRotator ViewRotation;
	PrimaryPC->GetPlayerViewPoint(ViewLocation, ViewRotation);

	FHitResult HitResult;
	FCollisionQueryParams QueryParams;
	QueryParams.bTraceComplex = true;
	APawn* PrimaryPawn = PrimaryPC->GetPawn();
	if (PrimaryPawn)
	{
		QueryParams.AddIgnoredActor(PrimaryPawn);
	}

	bool TraceResult = GetWorld()->LineTraceSingleByChannel(
		HitResult,
		ViewLocation,
		ViewLocation + ViewRotation.Vector() * 5000.0f,
		ECollisionChannel::ECC_Visibility,
		QueryParams
	);

	if (TraceResult)
	{
		out_LookingPoint = HitResult.ImpactPoint;
	}

	return TraceResult;
}
