// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerEffectExecutionCalculation.h"
#include "BaseAttributeSet.h"

UPlayerEffectExecutionCalculation::UPlayerEffectExecutionCalculation()
{
	//宏定义
	DEFINE_ATTRIBUTE_CAPTUREDEF(UBaseAttributeSet, Attack, Target, true)
	DEFINE_ATTRIBUTE_CAPTUREDEF(UBaseAttributeSet, Armor, Target, true)
	DEFINE_ATTRIBUTE_CAPTUREDEF(UBaseAttributeSet, Health, Target, true)
	//添加到捕获列表
	RelevantAttributesToCapture.Add(AttackDef);
	RelevantAttributesToCapture.Add(ArmorDef);
	RelevantAttributesToCapture.Add(HealthDef);
}

void UPlayerEffectExecutionCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	//计算捕获的属性
	float AttackMagnitude, ArmorMagnitude = 0.0f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(AttackDef, FAggregatorEvaluateParameters(), AttackMagnitude);
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(ArmorDef, FAggregatorEvaluateParameters(), ArmorMagnitude);
	//计算最终伤害
	float FinalDamage = FMath::Clamp(AttackMagnitude - ArmorMagnitude, 0.0f, AttackMagnitude - ArmorMagnitude);

	OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(HealthProperty, EGameplayModOp::Additive, -FinalDamage));
}
