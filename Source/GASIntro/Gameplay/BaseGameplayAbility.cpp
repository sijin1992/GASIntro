// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseGameplayAbility.h"

FGameplayAbilityInfo::FGameplayAbilityInfo()
	:Cooldown(0),
	Cost(0),
	CostType(ECostType::Mana),
	IconMaterial(nullptr),
	AbilityClass(nullptr)

{

}

FGameplayAbilityInfo::FGameplayAbilityInfo(float InCooldown, float InCost, ECostType InCostType, UMaterialInstance* InIconMaterial, TSubclassOf<UBaseGameplayAbility> InAbilityClass)
	: Cooldown(InCooldown),
	Cost(InCost),
	CostType(InCostType),
	IconMaterial(InIconMaterial),
	AbilityClass(InAbilityClass)
{

}

FGameplayAbilityInfo UBaseGameplayAbility::GetAbilityInfo()
{
	UGameplayEffect* CooldownEffect = GetCooldownGameplayEffect();
	UGameplayEffect* CostEffect = GetCostGameplayEffect();

	float Cooldown = 0;
	float Cost = 0;
	ECostType CostType = ECostType::Mana;

	if (CooldownEffect && CostEffect)
	{
		//获取1级的CD
		CooldownEffect->DurationMagnitude.GetStaticMagnitudeIfPossible(1, Cooldown);

		//判断是否应用了修改器
		if (CostEffect->Modifiers.Num() > 0)
		{
			//获取1级的消耗
			FGameplayModifierInfo CostEffectModifierInfo = CostEffect->Modifiers[0];
			CostEffectModifierInfo.ModifierMagnitude.GetStaticMagnitudeIfPossible(1, Cost);

			//获取消耗类型
			FGameplayAttribute CostAttribute = CostEffectModifierInfo.Attribute;
			FString CostAttributeName = CostAttribute.AttributeName;
			if (CostAttributeName == "Health")
			{
				CostType = ECostType::Health;
			}
			else if (CostAttributeName == "Mana")
			{
				CostType = ECostType::Mana;
			}
			else if (CostAttributeName == "Strength")
			{
				CostType = ECostType::Strength;
			}
		}
		return FGameplayAbilityInfo(Cooldown, Cost, CostType, IconMaterial, GetClass());
	}

	return FGameplayAbilityInfo();
}
