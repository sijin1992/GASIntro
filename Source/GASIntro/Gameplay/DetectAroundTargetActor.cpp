// Fill out your copyright notice in the Description page of Project Settings.


#include "DetectAroundTargetActor.h"
#include "GameFramework/PlayerController.h"
#include "Abilities/GameplayAbility.h"

void ADetectAroundTargetActor::StartTargeting(UGameplayAbility* Ability)
{
	Super::StartTargeting(Ability);

	PrimaryPC = Cast<APlayerController>(Ability->GetOwningActorFromActorInfo()->GetInstigatorController());
}

void ADetectAroundTargetActor::ConfirmTargetingAndContinue()
{
	APawn* PrimaryPawn = PrimaryPC->GetPawn();
	if (!PrimaryPawn)
	{
		return;
	}

	FVector PawnLocation = PrimaryPawn->GetActorLocation();

	TArray<FOverlapResult> OverlapResults;
	TArray<TWeakObjectPtr<AActor>> OverlapedActors;

	FCollisionQueryParams QueryParams;
	QueryParams.bTraceComplex = false;
	QueryParams.bReturnPhysicalMaterial = false;
	if (PrimaryPawn)
	{
		QueryParams.AddIgnoredActor(PrimaryPawn);
	}

	bool QueryResult = GetWorld()->OverlapMultiByObjectType(
		OverlapResults,
		PawnLocation,
		FQuat::Identity,
		FCollisionObjectQueryParams(ECollisionChannel::ECC_Pawn),
		FCollisionShape::MakeSphere(DetectRadius),
		QueryParams
	);

	if (QueryResult)
	{
		for (int i = 0; i < OverlapResults.Num(); i++)
		{
			APawn* OverlappedPawn = Cast<APawn>(OverlapResults[i].GetActor());

			if (OverlappedPawn && !OverlapedActors.Contains(OverlappedPawn))
			{
				OverlapedActors.Add(OverlappedPawn);
			}
		}
	}

	//仿写父类方法
	FGameplayAbilityTargetDataHandle TargetDataHandle;

	if (OverlapedActors.Num() > 0)
	{
		FGameplayAbilityTargetData_ActorArray* ActorArray = new FGameplayAbilityTargetData_ActorArray();
		ActorArray->SetActors(OverlapedActors);
		TargetDataHandle.Add(ActorArray);
	}

	check(ShouldProduceTargetData());
	if (IsConfirmTargetingAllowed())
	{
		TargetDataReadyDelegate.Broadcast(TargetDataHandle);
	}
}